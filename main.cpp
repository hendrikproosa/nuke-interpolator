// Point data interpolator
// Possible source for input is Syntheyes Export Tracker 2D data exporter
// which gives both 2D position and reprojected position

// Use the Windows 7 SDK MSVC 2010 x64 compiler

#define MAIN

static const char* const CLASS = "Interpolator";
static const char* const HELP = "Interpolates point data from input values";

// Base class for drawing
#include "DDImage/Iop.h"
#include "DDImage/DrawIop.h"
#include "DDImage/PixelIop.h"

#include "DDImage/Knobs.h"
#include "DDImage/DDMath.h"

#include "DDImage/Knob.h"
#include "DDImage/Channel3D.h"
#include "DDImage/Interest.h"
#include "DDImage/Row.h"
#include "DDImage/Tile.h"
#include "DDImage/ChannelSet.h"
#include "DDImage/Hash.h"
#include "DDImage/NukeWrapper.h"
#include <assert.h>
#include <fstream>

static const char* const datalabels[] = { "X Y Values", "ID X Y Values", "ID Time X Y Values", "X Y", "Nuke ascii tracker data", 0 };
static const char* const interpolationlabels[] = { "IDW", "Voronoi", "Distance", 0 };
static const char* const separators[] = { "comma", "semicolon", "space", 0 };

struct DataPoint {
    std::string id;
    int time;
    float x;
    float y;
    float value;
    float weight;
    std::vector<float> values;
};

class Interpolator : public DD::Image::Iop
{
private:
    // bounding box of the interpolated field
    bool useAVX;

    const char* filename;
    bool dataLoaded;
    bool pointsLoaded;
    bool sampleinput;
    bool samplingdone;
    int dataformat;
    int numvalues;
    int separator;
    int interpolationalgorithm;
    float sensitivity;
    bool usetime;
    int fadetime;
    int timeoffset;
    bool showpoints;
    float pointsize;
    std::vector<DataPoint> data;
    std::vector<DataPoint> pointData;
    std::vector<DataPoint> currentData;
    int minX, minY, maxX, maxY;
    bool point01_enabled;
    float point01_pos[2];
    float point01_color[4];
    float point01_weight;
    bool point02_enabled;
    float point02_pos[2];
    float point02_color[4];
    float point02_weight;
    bool point03_enabled;
    float point03_pos[2];
    float point03_color[4];
    float point03_weight;
    bool point04_enabled;
    float point04_pos[2];
    float point04_color[4];
    float point04_weight;

public:
    int maximum_inputs() const { return 1; }
    int minimum_inputs() const { return 1; }

    void _validate(bool);
    void _request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count);
    void engine ( int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& out );

    void append(DD::Image::Hash& hash);

    // make sure that all members are initialised
    Interpolator(Node* node) : Iop(node)
    {
        useAVX = false;
        filename = "";
        dataLoaded = false;
        pointsLoaded = false;
        sampleinput = false;
        samplingdone = false;
        numvalues = 1;
        sensitivity = 4.0;
        dataformat = 0;
        separator = 0;
        interpolationalgorithm = 0;
        usetime = false;
        fadetime = 0;
        timeoffset = 0;
        showpoints = false;
        pointsize = 10.0;
        minX = maxX = minY = maxY = 0;

        point01_enabled = true;
        point01_pos[0] = point01_pos[1] = 0.0f;
        point01_color[0] = point01_color[1] = point01_color[2] = point01_color[3] = 0.0f;
        point01_weight = 1.0;

        point02_enabled = true;
        point02_pos[0] = point02_pos[1] = 0.0f;
        point02_color[0] = point02_color[1] = point02_color[2] = point02_color[3] = 0.0f;
        point02_weight = 1.0;
    }

    virtual void knobs(DD::Image::Knob_Callback);
    int knob_changed(DD::Image::Knob* k);

    const char* Class() const { return CLASS; }
    const char* node_help() const { return HELP; }
    const char* displayName() const { return "Interpolator"; }
    static const Iop::Description description;

    bool readDataFile();
    bool readPoints();
    void updateCurrentPoints();
    void updateBounds();
    float interpolate(int method, float posx, float posy, int currentValue, float *values);
    float nanKiller(float value, float currentValue);
    float interpolateIDW(float posx, float posy, int currentValue);
    float interpolateVoronoi(float posx, float posy, int currentValue);
    float interpolateDistance(float posx, float posy, int currentValue);
};


void Interpolator::knobs(DD::Image::Knob_Callback f)
{
    DD::Image::Bool_knob(f, &useAVX, "use_AVX", "Use SIMD AVX");
    DD::Image::File_knob(f, &filename, "input_data", "Input data");
    DD::Image::Button(f, "load_data", "Reload data");

    DD::Image::Enumeration_knob(f, &dataformat, datalabels, "data_format", "Data format");
    DD::Image::Enumeration_knob(f, &separator, separators, "separator", "Separator");
    Divider(f, "Interpolation");
    DD::Image::Bool_knob(f, &sampleinput, "sample_input", "Sample input data");
    DD::Image::Tooltip(f, "Sample values from input image using point locations");
    DD::Image::Newline(f);
    DD::Image::Enumeration_knob(f, &interpolationalgorithm, interpolationlabels, "interp_mode", "Algorithm");
    DD::Image::Float_knob(f, &sensitivity, "sensitivity", "Sensitivity");
    DD::Image::SetRange(f, 0.0, 16.0);
    DD::Image::Tooltip(f, "IDW sensitivity, large values approach Voronoi cell look");
    Divider(f, "Time");
    DD::Image::Bool_knob(f, &usetime, "use_time", "Use time");
    DD::Image::Tooltip(f, "Use time field to animate points");
    DD::Image::Int_knob(f, &timeoffset, "time_offset", "Time offset");
    DD::Image::Tooltip(f, "Offset point time, useful for example when input data is Nuke tracker data without frame numbers");
    DD::Image::Int_knob(f, &fadetime, "fade_time", "Fade in/out time");
    Divider(f, "Points");
    DD::Image::Bool_knob(f, &showpoints, "show_points", "Show points only");
    DD::Image::Float_knob(f, &pointsize, "point_size", "Point size");
    DD::Image::SetRange(f, 0.1, 50.0);
    Divider(f);

    // Separate tab group for additional points
    DD::Image::Tab_knob(f, 1, "Points");
    Divider(f, "Point 01");
    DD::Image::Bool_knob(f, &point01_enabled, "point01_enabled", "Use point");
    DD::Image::XY_knob(f, point01_pos, "point01_pos", "Position");
    DD::Image::AColor_knob(f, point01_color, "point01_color", "Color");
    DD::Image::Float_knob(f, &point01_weight, "point01_weight", "Weight");
    Divider(f, "Point 02");
    DD::Image::Bool_knob(f, &point02_enabled, "point02_enabled", "Use point");
    DD::Image::XY_knob(f, point02_pos, "point02_pos", "Position");
    DD::Image::AColor_knob(f, point02_color, "point02_color", "Color");
    DD::Image::Float_knob(f, &point02_weight, "point02_weight", "Weight");
    Divider(f, "Point 03");
    DD::Image::Bool_knob(f, &point03_enabled, "point03_enabled", "Use point");
    DD::Image::XY_knob(f, point03_pos, "point03_pos", "Position");
    DD::Image::AColor_knob(f, point03_color, "point03_color", "Color");
    DD::Image::Float_knob(f, &point03_weight, "point03_weight", "Weight");
    Divider(f, "Point 04");
    DD::Image::Bool_knob(f, &point04_enabled, "point04_enabled", "Use point");
    DD::Image::XY_knob(f, point04_pos, "point04_pos", "Position");
    DD::Image::AColor_knob(f, point04_color, "point04_color", "Color");
    DD::Image::Float_knob(f, &point04_weight, "point04_weight", "Weight");


    Divider(f);
}


void Interpolator::_validate(bool for_real)
{
    copy_info();

    if (!dataLoaded)
        readDataFile();

    updateCurrentPoints();
    updateBounds();
}


void Interpolator::_request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count)
{
    if (sampleinput && dataLoaded)
    {
        input(0)->request( minX , minY , maxX, maxY, channels, count );
    } else {
        input(0)->request( x , y , r, t, channels, count );
    }
}


int Interpolator::knob_changed(DD::Image::Knob* k)
{
    if(k->is("input_data")) {
        dataLoaded = false;
        return true;
//        bool result = readDataFile();
//        return result;
    }

    if(k->is("load_data")) {
        dataLoaded = false;
        return true;
//        bool result = readDataFile();
//        return result;
    }

    if(k->is("data_format")) {
        dataLoaded = false;
        return true;
//        bool result = readDataFile();
//        return result;
    }

    if(k->is("separator")) {
        dataLoaded = false;
        return true;
//        bool result = readDataFile();
//        return result;
    }

    return DD::Image::Iop::knob_changed(k);
}


void Interpolator::append(DD::Image::Hash& hash)
{
    // If we read animated point data, append frame to hash
    // so that Nuke updates on every frame
    if (usetime)
        hash.append(outputContext().frame());
}


bool Interpolator::readDataFile()
{
    data.clear();
    dataLoaded = false;
    if (filename == "")
        return dataLoaded;

    std::ifstream infile(filename);

    // Check for file accessibility
    if (!infile.good())
    {
        infile.close();
        return dataLoaded;
    }

    int lineNumber = 0;
    std::string line;
    while (std::getline(infile, line))
    {
        std::vector<std::string> strings;
        std::istringstream iss(line);
        std::string s;
        switch (separator) {
            case 1:
                while (std::getline(iss, s, ';')) {
                    strings.push_back(s);
                }
                break;
            case 2:
                while (std::getline(iss, s, ' ')) {
                    strings.push_back(s);
                }
                break;
            default:
                while (std::getline(iss, s, ',')) {
                    strings.push_back(s);
                }
        }

        if (strings.size() < 2)
            continue;

        DataPoint pnt;
        pnt.time = 0;
        switch (dataformat) {
            case 1:
                if (strings.size() < 4)
                    break;
                pnt.id = strings.at(0);
                pnt.x = std::stof(strings.at(1));
                pnt.y = std::stof(strings.at(2));
                numvalues = strings.size() - 3;
                for (int i = 0; i < numvalues; ++i)
                {
                    pnt.values.push_back(std::stof(strings.at(3 + i)));
                }
                break;
            case 2:
                if (strings.size() < 5)
                    break;
                pnt.id = strings.at(0);
                pnt.time = std::stoi(strings.at(1));
                pnt.x = std::stof(strings.at(2));
                pnt.y = std::stof(strings.at(3));
                numvalues = strings.size() - 4;
                for (int i = 0; i < numvalues; ++i)
                {
                    pnt.values.push_back(std::stof(strings.at(4 + i)));
                }
                break;
            case 3:
                if (strings.size() < 2)
                    break;
                pnt.x = std::stof(strings.at(0));
                pnt.y = std::stof(strings.at(1));
                numvalues = 3;
                for (int i = 0; i < numvalues; ++i)
                {
                    pnt.values.push_back(1.0);
                }
            case 4:
                if (strings.size() < 2)
                    break;
                // First we do one point
                pnt.x = std::stof(strings.at(0));
                pnt.y = std::stof(strings.at(1));
                pnt.time = lineNumber;
                numvalues = 3;
                for (int i = 0; i < numvalues; ++i)
                {
                    pnt.values.push_back(1.0);
                }

                // And then the others if there are any
                for (int i = 1; i < strings.size() / 2; ++i)
                {
                    DataPoint pnt0;
                    pnt0.x = std::stof(strings.at(i * 2));
                    pnt0.y = std::stof(strings.at(i * 2 + 1));
                    // As tracker data has no frame column, give frame numbers based on current line
                    // We can use time offset setting to move the animation in time
                    pnt0.time = lineNumber;
                    for (int i = 0; i < numvalues; ++i)
                    {
                        pnt0.values.push_back(1.0);
                    }
                    data.push_back(pnt0);
                }

            default:
                if (strings.size() < 3)
                    break;
                pnt.x = std::stof(strings.at(0));
                pnt.y = std::stof(strings.at(1));
                numvalues = strings.size() - 2;
                for (int i = 0; i < numvalues; ++i)
                {
                    pnt.values.push_back(std::stof(strings.at(2 + i)));
                }
        }
        data.push_back(pnt);
        lineNumber++;
    }

    if (!data.size())
        return false;

    currentData = data;
    dataLoaded = true;
    return true;
}


bool Interpolator::readPoints()
{
    // Check for additional points and add enabled points to data
    bool pointsenabled = false;
    pointData.clear();

    if (point01_enabled)
    {
        pointsenabled = true;
        DataPoint pnt;
        pnt.time = outputContext().frame() + timeoffset;
        pnt.weight = point01_weight;
        pnt.x = point01_pos[0];
        pnt.y = point01_pos[1];
        for (int i = 0; i < 4; ++i)
        {
            pnt.values.push_back(point01_color[i]);
        }
        pointData.push_back(pnt);
    }
    if (point02_enabled)
    {
        pointsenabled = true;
        DataPoint pnt;
        pnt.time = outputContext().frame() + timeoffset;
        pnt.weight = point02_weight;
        pnt.x = point02_pos[0];
        pnt.y = point02_pos[1];
        for (int i = 0; i < 4; ++i)
        {
            pnt.values.push_back(point02_color[i]);
        }
        pointData.push_back(pnt);
    }
    if (point03_enabled)
    {
        pointsenabled = true;
        DataPoint pnt;
        pnt.time = outputContext().frame() + timeoffset;
        pnt.weight = point03_weight;
        pnt.x = point03_pos[0];
        pnt.y = point03_pos[1];
        for (int i = 0; i < 4; ++i)
        {
            pnt.values.push_back(point03_color[i]);
        }
        pointData.push_back(pnt);
    }
    if (point04_enabled)
    {
        pointsenabled = true;
        DataPoint pnt;
        pnt.time = outputContext().frame() + timeoffset;
        pnt.weight = point04_weight;
        pnt.x = point04_pos[0];
        pnt.y = point04_pos[1];
        for (int i = 0; i < 4; ++i)
        {
            pnt.values.push_back(point04_color[i]);
        }
        pointData.push_back(pnt);
    }

    dataLoaded = pointsenabled;
    return pointsenabled;
}


void Interpolator::updateBounds()
{
    if (!dataLoaded)
        return;

    if (currentData.size())
    {
        DataPoint pnt0 = currentData.at(0);
        minX = floor(pnt0.x);
        minY = floor(pnt0.y);
        maxX = ceil(pnt0.x);
        maxY = ceil(pnt0.y);
        for (int i = 0; i < currentData.size(); ++i)
        {
            DataPoint pnt = currentData.at(i);
            if (floor(pnt.x) < minX)
                minX = floor(pnt.x);
            if (floor(pnt.y) < minY)
                minY = floor(pnt.y);
            if (floor(pnt.x) > maxX)
                maxX = floor(pnt.x);
            if (floor(pnt.y) > maxY)
                maxY = floor(pnt.x);
        }
    }
}


void Interpolator::updateCurrentPoints()
{
    currentData.clear();
    readPoints();

    // If we have time in our data file, sort out points that are enabled at current frame
    if (usetime)
    {
        for (int i = 0; i < data.size(); ++i)
        {
            DataPoint pnt = data.at(i);
            if (pnt.time == outputContext().frame() + timeoffset)
                currentData.push_back(pnt);
        }
        for (int i = 0; i < pointData.size(); ++i)
        {
            DataPoint pnt = pointData.at(i);
            if (pnt.time == outputContext().frame() + timeoffset)
                currentData.push_back(pnt);
        }

    } else {
        currentData = data;
        for (int i = 0; i < pointData.size(); ++i)
        {
            DataPoint pnt = pointData.at(i);
            currentData.push_back(pnt);
        }
    }
}


void Interpolator::engine (int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& row)
{
    int minXc, minYc, maxXc, maxYc;
    minXc = x;
    maxXc = r;
    minYc = maxYc = y;
    if (sampleinput && dataLoaded)
    {
        minXc = minX;
        minYc = minY;
        maxXc = maxX;
        maxYc = maxY;
    }
    // Make a tile for current line with padding arond for sampling the input image
    DD::Image::Tile tile( input0(), minXc , minYc , maxXc, maxYc , channels);
    if (aborted())
        return;


    // /////////////////////////////////////////////
    // Input image sampling for interpolation data
    // /////////////////////////////////////////////

    if (sampleinput && dataLoaded)
    {
        // Sample the location of each input point first
        // We use these values later for interpolation
        int currentchannel = 0;
        foreach ( z, channels ) {
            if ( intersect( tile.channels(), z ) ) {
                for (int i = 0; i < currentData.size(); ++i)
                {
                    DataPoint pnt = currentData.at(i);
                    float sampledValue = tile[z][ tile.clampy((int)pnt.y)][ tile.clampx((int)pnt.x) ];
                    if (pnt.values.size() > currentchannel)
                    {
                        currentData.at(i).values.at(currentchannel) = sampledValue;
                    } else {
                        currentData.at(i).values.push_back(sampledValue);
                    }
                }
            }
            currentchannel++;
        }
    }

    // /////////////////////////////////////////////
    // Actual workload
    // /////////////////////////////////////////////

    // Normal interpolation from input data
    int currentchannel = 0;
    foreach ( z, channels ) {
        float* outptr = row.writable(z) + x;
        for( int cur = x ; cur < r; cur++ ) {
            float value = 0.0;
            if (dataLoaded)
                value = interpolate(interpolationalgorithm, cur, y, currentchannel, outptr);
            *outptr++ = value;
        }
        currentchannel++;
    }

    return;

    // Optimized version
    // Buggy as hell, crashes

    // We want to calculate the distances only once for all the channels because distance does not change
    // We want to use the fastest possible distance calculation method, octagonal approximation:
    // dx = |x1 - x0|
    // dy = |y1 - y0|
    // if dy > dx:
    //   dist = 0.41dx + 0.941246dy

    // To get best possible performance out of this, we must calculate the distances row-wise, so that we get distances by one addition ONLY
    // As deltas change, we first find the areas where dy > dx and where it is not and sepate them so we don't do comparison for every pixel

    // Pre-compute distances or weights for whole row in advance
    // Use the pre-computed weights to fill array with sum of weights. This array is the same for all channels
    // Use the pre-computed weights to fill another array with weighted value sum, this varies for data channels


    // First, iterate over full scanline
    // Here we fill the data structures that are used for all operations and/or multiple times

    // For every point, we need to know the distance to it from every pixel.
    // To do this, we allocate memory chunk with the size of N points multiplied by row length.
    // We pad the row length to multiple of 8 to use 256-bit AVX vector ops
    int paddedRowLength = ((r - x) / 8 + 1) * 8;
    float* distanceCache = (float*)malloc( paddedRowLength * sizeof( float ) * (int)currentData.size());
    float* weightCache = (float*)malloc( paddedRowLength * sizeof( float ) * (int)currentData.size());

    // We use this pointer to keep track of offset in distance cache for each data point
    float* dataPointerDistance = distanceCache;
    float* dataPointerWeight = weightCache;
    // Bring initialization out from loops
    float distance = 0.0;
    float weight = 0.0;
    float px = 0.0;
    float py = 0.0;
    float adx = 0.0;
    float dx = 0.0;
    float dy = 0.0;
    float dy_term0 = 0.0;
    float dy_term1 = 0.0;
    float* rowPointerDistance = nullptr;
    float* rowPointerWeight = nullptr;
    float* rowPointerSumOfWeights = nullptr;

    float neg_half_sens = -sensitivity/2.0;

    // Iterate over points
    for (int i = 0; i < currentData.size(); ++i)
    {
        DataPoint pnt = currentData.at(i);
        px = pnt.x;
        py = pnt.y;
        // Delta x will start possibly negative, so we can simply increment it
        dx = x - px;
        // Delta y will be constant for whole row
        dy = abs(py - y);
        dy_term0 = 0.941246f * dy;
        dy_term1 = 0.41f * dy;
        rowPointerDistance = dataPointerDistance;
        rowPointerWeight = dataPointerWeight;
        for (int cur = x; cur < r; cur++)
        {
            adx = abs(dx);
            // Calculate the distance and weight
            if (dy > adx)
            {
                distance = 0.41f * adx + dy_term0;
            } else{
                distance = dy_term1 + 0.941246f * adx;
            }
            weight = powf(distance * distance, neg_half_sens);
            *rowPointerDistance = distance;
            *rowPointerWeight = weight;

            // Increment the incrementals
            dx = dx + 1.0f;
            rowPointerDistance++;
            rowPointerWeight++;
        }
        // Change the datapointer offset
        dataPointerDistance = dataPointerDistance + paddedRowLength;
        dataPointerWeight = dataPointerWeight + paddedRowLength;
    }


    // Iterate over row again and produce sum of weights and proportional weight values
    float* offset = rowPointerWeight;
    for (int cur = x; cur < r; cur++)
    {
        for (int i = 0; i < currentData.size(); ++i)
        {
            rowPointerSumOfWeights[cur - x] = rowPointerSumOfWeights[cur - x] + distanceCache[i * paddedRowLength + cur - x];
        }
    }

    // Now iterate over input data
    float value = 0.0;
    float valuePrevious = 0.0;
    float weighted_values_sum = 0.0;
    float sum_of_weights = 0.0;


    currentchannel = 0;
    foreach ( z, channels ) {
        float* outptr = row.writable(z) + x;
        for( int cur = x ; cur < r; cur++ ) {
            weighted_values_sum = 0.0;
            sum_of_weights = 0.0;

            for (int i = 0; i < currentData.size(); ++i)
            {
                distance = distanceCache[i * paddedRowLength + cur - x];
                weight = weightCache[i * paddedRowLength + cur - x];
                DataPoint pnt = currentData.at(i);
                // Check that we have value for this index in vector to not violate vector boundary
                if (pnt.values.size() > currentchannel)
                {
                    //float weight = powf((powf(posx-pnt.x, 2) + powf(posy-pnt.y, 2)), neg_half_sens);
                    //float weight = powf(distance * distance, neg_half_sens);

                    // Sum of weights can be known beforehand
                    // From sum of weights we can calculate the proportion of current weight beforehand
                    // Only thing we need to do after that is multiply proportional weight by value

                    sum_of_weights += weight;
                    weighted_values_sum += weight * pnt.values.at(currentchannel);
                }
            }
            value = weighted_values_sum / sum_of_weights;

            if (isnan(value))
            {
                value = *(outptr - 1);
            }

            *outptr++ = value;
            valuePrevious = value;
        }
        currentchannel++;
    }

    free(distanceCache);
    free(weightCache);

}


float Interpolator::interpolate(int method, float posx, float posy, int currentValue, float* values)
{
    double result = 0.0;

    // If we render the points, there is nothing to interpolate
    // We return the value after checking against all points
    if (showpoints)
    {
        if (useAVX)
        {
            // Create float arrays to hold data point coordinates and values
            // arrays are padded to multiple of 8 to use 256-bit vector ops
            int paddedSize = ((int)currentData.size() / 8 + 1) * 8;
            float* cdX = (float*)malloc( paddedSize * sizeof( float ));
            float* cdY = (float*)malloc( paddedSize * sizeof( float ));
            float* cdV = (float*)malloc( paddedSize * sizeof( float ));
            float* cdD = (float*)malloc( paddedSize * sizeof( float ));

            // Fill float arrays with values from point data
            for (int i = 0; i < currentData.size(); ++i)
            {
                DataPoint pnt = currentData.at(i);
                cdX[i] = pnt.x;
                cdY[i] = pnt.y;
                cdV[i] = pnt.values.at(currentValue);
                cdD[i] = 0.0;
            }

            __m256 x0, x1, y0, y1;
            __m256 v;

            for (int i = 0; i < paddedSize; i+=8)
            {
                // Initialize input point coordinates to vector
                x0 = _mm256_set1_ps(posx);
                y0 = _mm256_set1_ps(posy);

                // Move floats from data point arrays to vector
                x1 = _mm256_load_ps(&cdX[i]);
                y1 = _mm256_load_ps(&cdY[i]);
                v = _mm256_load_ps(&cdV[i]);

                // Calculate distance
                __m256 dx, dy;
                __m256 dx2, dy2;
                __m256 d2sum, d2sqrt;
                dx = _mm256_sub_ps(x0, x1);
                dy = _mm256_sub_ps(y0, y1);
                dx2 = _mm256_mul_ps(dx, dx);
                dy2 = _mm256_mul_ps(dy, dy);
                d2sum = _mm256_add_ps(dx2, dy2);
                d2sqrt = _mm256_sqrt_ps(d2sum);

                // Store calculated distances back to float array
                // Must use unaligned store here, otherwise we get crash
                _mm256_storeu_ps(&cdD[i], d2sqrt);
            }

            for (int i = 0; i < currentData.size(); ++i)
            {
                if (cdD[i] < pointsize)
                {
                    DataPoint pnt = currentData.at(i);
                    if (pnt.values.size() > currentValue)
                        result = pnt.values.at(currentValue);
                }
            }

            // Free allocated memory
            free(cdX);
            free(cdY);
            free(cdV);
            free(cdD);

            return result;

        } else {
            for (int i = 0; i < currentData.size(); ++i)
            {
                DataPoint pnt = currentData.at(i);
                if (powf(powf((posx - pnt.x), 2) + powf((posy - pnt.y), 2), 0.5) < pointsize)
                {
                    if (pnt.values.size() > currentValue)
                        result = pnt.values.at(currentValue);
                }
            }
            return result;
        }
    }

    switch (method) {
    case 1:
        result = interpolateVoronoi(posx, posy, currentValue);
        break;
    case 2:
        result = interpolateDistance(posx, posy, currentValue);
        break;
    default:
        result = interpolateIDW(posx, posy, currentValue);
        break;
    }

    return result;
}


float Interpolator::nanKiller(float value, float currentValue)
{
    float result = value;
    if (isnan(result))
    {
        for (int i = 0; i < currentData.size(); ++i)
        {
            DataPoint pnt = currentData.at(i);
            if (pnt.values.size() > currentValue)
            {
//                if (posx == floor(pnt.x) && posy == floor(pnt.y))
//                {
                    result = pnt.values.at(currentValue);
//                }
            }
        }
    }
    return result;
}

float Interpolator::interpolateIDW(float posx, float posy, int currentValue)
{
    float result = 0.0;
    float weighted_values_sum = 0.0;
    float sum_of_weights = 0.0;
    float neg_half_sens = -sensitivity/2.0;

    for (int i = 0; i < currentData.size(); ++i)
    {
        DataPoint pnt = currentData.at(i);
        // Check that we have value for this index in vector to not violate vector boundary
        if (pnt.values.size() > currentValue)
        {
            float weight = powf((powf(posx-pnt.x, 2) + powf(posy-pnt.y, 2)), neg_half_sens);
            sum_of_weights += weight;
            weighted_values_sum += weight * pnt.values.at(currentValue);
        }
    }
    result = weighted_values_sum / sum_of_weights;

    if (isnan(result))
    {
        for (int i = 0; i < currentData.size(); ++i)
        {
            DataPoint pnt = currentData.at(i);
            if (pnt.values.size() > currentValue)
            {
                if (posx == floor(pnt.x) && posy == floor(pnt.y))
                {
                    result = pnt.values.at(currentValue);
                }
            }
        }
    }
    return result;
}


float Interpolator::interpolateVoronoi(float posx, float posy, int currentValue)
{
    float result = 0.0;
    float minDistance = 10000.0;
    int minDistancePoint = 0;

    for (int i = 0; i < currentData.size(); ++i)
    {
        DataPoint pnt = currentData.at(i);
        if (pnt.values.size() > currentValue)
        {
            double distance = powf(powf((posx - pnt.x), 2) + powf((posy - pnt.y), 2), 0.5);
            if (distance < minDistance)
            {
                minDistance = distance;
                minDistancePoint = i;
            }
        }
    }

    if (currentData.at(minDistancePoint).values.size() > currentValue)
        result = currentData.at(minDistancePoint).values.at(currentValue);

    return result;
}


float Interpolator::interpolateDistance(float posx, float posy, int currentValue)
{
    float result = 0.0;
    float minDistance = 10000.0;

    for (int i = 0; i < currentData.size(); ++i)
    {
        DataPoint pnt = currentData.at(i);
        if (pnt.values.size() > currentValue)
        {
            double distance = powf(powf((posx - pnt.x), 2) + powf((posy - pnt.y), 2), 0.5);
            if (distance < minDistance)
            {
                minDistance = distance;
            }
        }
    }

    result = minDistance;
    return result;
}


static DD::Image::Iop* build(Node* node)
{
    return (new DD::Image::NukeWrapper(new Interpolator(node)))->channelsRGBoptionalAlpha();
}
const DD::Image::Op::Description Interpolator::description(CLASS, "Image/Draw/Interpolator", build);

