# Point data interpolator for Nuke 10
## Created by Hendrik Proosa

Description based on version built May 3, 2019

Currently tested in 10.5

--------------------------------------------------------
What it does
--------------------------------------------------------
Interpolator generates an image based on point data. Currently it is possible to use Inverse Distance Weighting,
Voronoi cells and Distance to closest point. In addition to reading interpolated values from file, you can sample the
input image values. If point data has time values, it is possible to use animated points. Fade in/out not working yet.


--------------------------------------------------------
How to use
--------------------------------------------------------
**Channels**
	where to write the interpolated data. Channels must exist in pipe before, so for example to
	write into UV channels, use AddChannels node to create these first if they don't exist in input.

**Input data**
	any text file format goes, csv, txt etc. No headers please currently!

**Data format**
	how the data is arranged in rows. Values can be an arbitrary number of values,
	but each row should have same numberof values!

**Separator**
	field separator in file. Comma, semicolon and space currently supported.

**Sample input data**
	sample channel values from input image instead of using values in file.

**Algorithm**
	interpolation algo to use.
	IDW is Shepard's Inverse Distance Weighting.
	Voronoi is voronoi cells (value of closest point)
	Distance is distance in pixels to closest point (absolute value in pixel width)

**Sensitivity**
	Sensitivity parameter for IDW. Smaller values make sharper falloff, large values result in image similar to voronoi.
	Value of 4.0 is nice start for smooth interpolation

**Use time**
	If checked, only points that have time value equal to current frame are used in interpolation. Effectively produces animated points.

**Time offset**
	Slide animation in time. Useful when importing points with missing frame number field.

**Fade in/out time**
	Idea is to avoid pops when points appear in certain frames by fading their weight in and out over a range of frames. Not working yet.

**Show points only**
	Renders points as spheres (not antialiased currently). If channels set is rgba and input pipe has alpha channel, also fills alpha.

**Point size**
	Radius of points when rendered.
    
**Points tab**
    Second tab besides Interpolator main tab is additional points tab. Currently it has limited set of points, but I'll think about dynamic knob creation. These points work in addition to loaded data, so even if nothing is loaded, they can still be used.
    


--------------------------------------------------------
Interesting stuff to try
--------------------------------------------------------
1. Use tracker data
Export tracker points as ascii file and use Nuke ascii tracker data format to read that file in. Voila, moving points!
Use time offset value to slide them in time (ascii export does not write frame numbers for some reason so I start frames from 0)

2. Correct residual errors in matchmoving
Export 2d tracker locations from Syntheyes or some other software so that you have both 2d feature location and reprojected location.
With some fiddling in Excel or GSheets, create a file where you have tracker ID-s, frame numbers, tracker locations and difference
between 2d position and reprojected position. Interpolate that difference with IDW and use it in IDistort node to warp your stuff into place!

3. Create some nice patterns
Use distance field as distortion map for STMap, this produces interesting patterns. Try checkerboard, waves and other stuff as input image.
Use voronoi as distortion map for IDistort, another set of interesting effects.

4. Create gradients for paint work
Track some points (in sky for example) and use IDW to build gradient map. Use the gradient as helper for paint fixes. 


--------------------------------------------------------
Input file format
--------------------------------------------------------
Currently supported formats:
X Y Values
	Point coordinates and an arbitrary number of attribute values

ID X Y Values
	Adds point ID, although the ID does not affect interpolation

ID Time X Y Values
	Adds time value, which is frame number. If "Use time" is enabled, only points that have current frame are used.

X Y
	Only point coordinates, nothing else. Useful for sampling input image or generating distance field.

Nuke ascii tracker data
	Tracker export data where tracker coordinates are in consecutive columns
	Frame range starts from 0, use time offset setting to slide animation in time

Example data for using time to animate points:
1;1;100;100;0.9;0.2;0.1
1;2;110;100;0.9;0.2;0.1
1;3;120;100;0.9;0.2;0.1
1;4;130;100;0.9;0.2;0.1
1;5;140;100;0.9;0.2;0.1
1;6;150;100;0.9;0.2;0.1
1;7;160;100;0.9;0.2;0.1
1;8;170;100;0.9;0.2;0.1
1;9;180;100;0.9;0.2;0.1
1;10;190;100;0.9;0.2;0.1
2;1;1800;750;0.1;0.3;0.8
2;2;1770;749;0.1;0.3;0.8
2;3;1740;748;0.1;0.3;0.8
2;4;1710;747;0.1;0.3;0.8
2;5;1680;746;0.1;0.3;0.8
2;6;1650;745;0.1;0.3;0.8
2;7;1620;744;0.1;0.3;0.8
2;8;1590;743;0.1;0.3;0.8
2;9;1560;742;0.1;0.3;0.8
2;10;1530;741;0.1;0.3;0.8

Here we have two points, with ID-s 1 and 2 and each has data for 10 frames. If "Use time" is enabled, we get two animated points :)


--------------------------------------------------------
Known bugs and other annoyances
--------------------------------------------------------
It is slow with more than a fewty points. I'll try to do something with it, maybe try some AVX vector magic.

It sometimes crashes when sampling from image data is enabled and there are more channels enabled than there are values in point data.

Sometimes cache does not update. Clear cache, use refresh frame button in viewer or press Reload data button.

The fade in/out is not currently working, so expect pops if points don't cover whole frame range.